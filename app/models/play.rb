class Play < ApplicationRecord
  belongs_to :user
  belongs_to :category
  has_many :reviews
  
  has_attached_file :play_img, 
                    :styles => { :play_index => "250x350>", :play_show => "325x475>" }, 
                    :default_url => "/images/:style/missing.png",
                    :storage => :s3,
                    :s3_credentials => {
                    :bucket  => ENV['AMAZON_S3_BUCKET_NAME'],
                    :access_key_id => ENV['AMAZON_ACCESS_KEY_ID'],
                    :secret_access_key => ENV['AMAZON_SECRET_ACCESS_KEY'] },
                    :path => ":attachment/:id/:style.:extension"
  validates_attachment_content_type :play_img, :content_type => /\Aimage\/.*\Z/
end
